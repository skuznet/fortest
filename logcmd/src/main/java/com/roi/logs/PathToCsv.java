package com.roi.logs;

public interface PathToCsv {
    String getConfigPathIn();
    String getConfigPathOut();
    String pathIn();
    String pathOut();
}
