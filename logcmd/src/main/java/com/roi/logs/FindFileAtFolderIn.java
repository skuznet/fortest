package com.roi.logs;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class FindFileAtFolderIn {
    File dir = new File(new PathToLogFolder().pathIn());
    public ArrayList<String> fileNames = new ArrayList<>();

    public void checkFileNamesIn() {
        Collections.addAll(fileNames, dir.list());
    }
}
