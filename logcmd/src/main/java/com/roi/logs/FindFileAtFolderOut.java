package com.roi.logs;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class FindFileAtFolderOut {
    File dir = new File(new PathToLogFolder().pathOut());
    public ArrayList<String> fileNames = new ArrayList<>();

    public void checkFileNamesOut() {
        Collections.addAll(fileNames, dir.list());
    }
}
