package com.roi.logs;

public class Site {
    private String url;
    private int averageTime;
    private int count;

    public Site(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getAverageTime() {
        return averageTime;
    }

    public void setAverageTime(int averageTime) {
        this.averageTime = averageTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Site)) return false;
        Site site = (Site) o;
        return getUrl() != null ? getUrl().equals(site.getUrl()) : site.getUrl() == null;

    }

    @Override
    public int hashCode() {
        return getUrl() != null ? getUrl().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Site{" +
                "url='" + url + '\'' +
                ", averageTime=" + averageTime +
                ", count=" + count +
                '}';
    }

    public int getCountTime() {
        return averageTime/count;
    }
}
