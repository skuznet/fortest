package com.roi.logs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class LogParse {
    BufferedReader fileReader = null;
    private final String DELIMITER = ",";
    String tokens;
    String[] arrayCsv;
    Set<User> colCsv;

    void receiveUniqUser(String path) {
        try {
            String line;
            //Create the file reader
            fileReader = new BufferedReader(new FileReader(path));
            //Read the file line by line
            colCsv = new HashSet<>();
            for (; (line = fileReader.readLine()) != null; ) {
                arrayCsv = line.split(DELIMITER);
                colCsv.add(new User(arrayCsv[1]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void addSiteToUser(String path) {
        receiveUniqUser(path);
        try {
            String line;
            fileReader = new BufferedReader(new FileReader(path));
            for (; (line = fileReader.readLine()) != null; ) {
                arrayCsv = line.split(DELIMITER);
                for (Iterator<User> it = colCsv.iterator(); it.hasNext(); ) {
                    User user = it.next();
                    if (user.equals(new User(arrayCsv[1]))) {
                        user.getSetSite().add(new Site(arrayCsv[2]));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void getAverageTime(String path) {
        addSiteToUser(path);
        try {
            String line;
            fileReader = new BufferedReader(new FileReader(path));
            for (; (line = fileReader.readLine()) != null; ) {
                arrayCsv = line.split(DELIMITER);
                for (Iterator<User> it = colCsv.iterator(); it.hasNext(); ) {
                    User user = it.next();
                    if (user.equals(new User(arrayCsv[1]))) {
                        for (Iterator<Site> iterat = user.getSetSite().iterator(); iterat.hasNext(); ) {
                            Site site = iterat.next();
                            if (site.equals(new Site(arrayCsv[2]))) {
                                site.setAverageTime(site.getAverageTime() + Integer.parseInt(arrayCsv[3]));
                                site.setCount(site.getCount() + 1);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void result(String path) {
        GetDate getDate = new GetDate();
        //System.out.println(getDate.getDate());
        tokens = getDate.getDate();
        //System.out.println(tokens);
        //System.out.println();
        tokens = tokens + "\n" + "\n";
        getAverageTime(path);
        for (User col : colCsv) {
            for (Site site : col.getSetSite()) {
                tokens = tokens  + col.getUsername() + "," + site.getUrl() + "," + site.getCountTime() + "\n";
                //System.out.println(col.getUsername() + "," + site.getUrl() + "," + site.getCountTime());
            }
        }
    }
}


