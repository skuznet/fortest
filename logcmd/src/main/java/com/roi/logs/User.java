package com.roi.logs;

import java.util.HashSet;

public class User {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String username;

    public HashSet<Site> getSetSite() {
        return setSite;
    }

    public void setSetSite(HashSet<Site> setSite) {
        this.setSite = setSite;
    }

    private HashSet<Site> setSite;

    public User(String username) {
        this.username = username;
        this.setSite = new HashSet<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", setSite=" + setSite +
                '}';
    }
}
