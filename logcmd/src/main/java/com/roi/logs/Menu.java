package com.roi.logs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Menu {
    public void menuView(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true)
        try {
            entryMessage();
            String readKey = bufferedReader.readLine();
            if(readKey.equals("1")){
                System.out.println(getHelp());
            }else if(readKey.equals("2")){
                Main.start();
                GetResultToView toView = new GetResultToView();
                FindFileAtFolderOut find = new FindFileAtFolderOut();
                find.checkFileNamesOut();
                String[] stockArr = new String[find.fileNames.size()];
                stockArr = find.fileNames.toArray(stockArr);
                for (int i = 0; i < find.fileNames.size(); i++) {
                    toView.resultToView(new PathToLogFolder().pathOut() + stockArr[i]);
                }
            } else if(readKey.toLowerCase().equals("exit")){
                break;
            }else{
                System.out.println("Please enter correct value.");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println();
    }

    public void entryMessage(){
        System.out.println("Welcome to our log parser program!\n\nMenu:\nPress 1: to enter help menu" +
        "\nPress 2: to run program\n\nEnter \"exit\": to quit.");
    }

    public String getHelp() {
        return ("Please make c:\\java\\csv and c:\\java\\out folders." +
                "\nPut your csv files what you want to parse to c:\\java\\csv folder." +
                "\nResult of work your program you can find at c:\\java\\out." +
                "\nIf you want change in and out folders - you can make this action at config.txt for input" +
                "\nand outfile.txt for output.");
    }
}
