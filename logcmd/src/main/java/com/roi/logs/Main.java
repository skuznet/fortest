package com.roi.logs;


import java.io.File;

public class Main implements Runnable {
    protected static final int THREADS = 10;

    private static class Semaphore {
        public boolean set = false;
    }

    protected static final Semaphore[] semaphores = new Semaphore[THREADS];
    protected static final Thread[] threads = new Thread[THREADS];
    protected final int threadNum;

    protected Main(int num) {
        this.threadNum = num;
    }

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.menuView();
    }

    public void run() { //todo need fix RuntimeException if files <=10
        FindFileAtFolderIn find = new FindFileAtFolderIn();
        find.checkFileNamesIn();
        String[] stockArr = new String[find.fileNames.size()];
        stockArr = find.fileNames.toArray(stockArr);
        for (int i = 0; i < find.fileNames.size(); i++) {
            synchronized (semaphores) {
                semaphores[this.threadNum].set = true;
                semaphores.notify();
            }
            // if(new File(new PathToLogFolder().pathIn() + stockArr[i]).exists()) {
            final long startExec = System.currentTimeMillis();
            LogParse logParse = new LogParse();
            logParse.result(new PathToLogFolder().pathIn() + stockArr[i]);
            WriteToFile writeToFile = new WriteToFile();
            try {
                writeToFile.write(new PathToLogFolder().pathOut() + "avg_" + stockArr[i], logParse.tokens);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            //if(exists(new PathToLogFolder().pathIn() + stockArr[i]);
            /*final long finishExec = System.currentTimeMillis();
            System.out.println("Thread N" + this.threadNum + ". Start: " +
                    startExec + ". End: " + finishExec +
                    ". Total: " + (finishExec - startExec) + ".");*/
        }
    }

    static void start() {
        FindFileAtFolderIn findFileAtFolder = new FindFileAtFolderIn();
        findFileAtFolder.checkFileNamesIn();
        for (int i = 0; i < THREADS; i++) {
            semaphores[i] = new Semaphore();
        }
        System.err.println("Starting " + THREADS + " threads...");
        synchronized (Main.class) {
            synchronized (semaphores) {
                for (int i = 0; i < THREADS; i++) {
                    final Main mainThread = new Main(i);
                    threads[i] = new Thread(mainThread, "Thread N" + i);
                    threads[i].setDaemon(true);
                    threads[i].start();
                }
                System.err.println("Waiting for simultaneous start...");
                boolean isAllStarted = false;
                while (!isAllStarted) {
                    try {
                        semaphores.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    isAllStarted = true;
                    for (int i = 0; i < THREADS; i++) {
                        if (!semaphores[i].set) {
                            isAllStarted = false;
                            break;
                        }
                    }
                }
            }
        }
        System.err.println("Waiting for threads to finish execution...");
        for (int i = 0; i < THREADS; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
        System.out.println("Program finished successfully.");
        FindFileAtFolderIn find = new FindFileAtFolderIn();
        find.checkFileNamesIn();
        String[] stockArr = new String[find.fileNames.size()];
        stockArr = find.fileNames.toArray(stockArr);
        for (String aStockArr : stockArr) {
            new File(new PathToLogFolder().pathIn() + aStockArr).delete();
        }
    }
}



