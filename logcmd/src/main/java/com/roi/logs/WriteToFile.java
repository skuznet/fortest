package com.roi.logs;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class WriteToFile {
    public void write(String fileName, String text) throws RuntimeException{
        //file identity
        File file = new File(fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            //for enable write to file
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try {
                //text to file
                out.print(text);
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
