package com.roi.logs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class PathToLogFolder implements PathToCsv {
    private String configPathIn = "config.txt";
    private String configPathOut = "outfile.txt";
    private String resultIn;
    private String resultOut;

    public String pathIn() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(getConfigPathIn()).getFile());
            BufferedReader reader = new BufferedReader(new FileReader(file));
            resultIn = reader.readLine();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return resultIn;
    }

    public String pathOut(){
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(getConfigPathOut()).getFile());
            BufferedReader reader = new BufferedReader(new FileReader(file));
            resultOut = reader.readLine();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return resultOut;
    }

    public String getConfigPathIn() {
        return configPathIn;
    }

    public String getConfigPathOut() {
        return configPathOut;
    }
}
