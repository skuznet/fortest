package com.roi.logs;

import java.util.Calendar;

public class GetDate {
    Calendar calendar = Calendar.getInstance();
    int year= calendar.get(Calendar.YEAR);
    int month= calendar.get(Calendar.MONTH)+1;
    int day= calendar.get(Calendar.DAY_OF_MONTH);

    String monthFromDigitalToString(){
        switch (month) {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AUG";
            case 9:
                return "SEN";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";
            default:
                return "wrong month";
        }
    }

    public String getDate(){
        return day + "-" + monthFromDigitalToString() + "-" + year;
    }
}
