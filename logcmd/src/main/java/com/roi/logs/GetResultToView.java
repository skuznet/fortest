package com.roi.logs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class GetResultToView {
    private static final String DELIMITER = ",";
    String[] lineArr;

    public void resultToView(String path) {
        BufferedReader fileReader = null;
        try {
            String line;
            fileReader = new BufferedReader(new FileReader(path));
            for (; (line = fileReader.readLine()) != null; ) {
                lineArr = line.split(DELIMITER);
                System.out.println(Arrays.toString(lineArr));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

