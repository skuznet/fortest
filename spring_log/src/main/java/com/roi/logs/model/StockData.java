package com.roi.logs.model;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.io.BufferedReader;
import java.io.FileReader;

public class StockData {
    private static String sqlCheck;
    private String fileUrl;
    private String line;
    private String cvsSplitBy;
    private JdbcTemplate jdbcTemplate;
    private SimpleDriverDataSource dataSource;

    public StockData(SimpleDriverDataSource dataSource, JdbcTemplate jdbcTemplate) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
    }

    public void sqlInsert() {
        try { // to update db...
            BufferedReader br = new BufferedReader(new FileReader(fileUrl));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] country = line.split(getCvsSplitBy());
                int id = Integer.parseInt(country[0]);
                int time = Integer.parseInt(country[3]);
                String name = country[1], url = country[2];
                String sql = ("INSERT INTO logs(ID, user, url, timespent) VALUES(?,?,?,?)");
                jdbcTemplate.update(sql, id, name, url, time);
            }
            sqlCheck = "db updated";
        } catch (Exception e) {
            sqlCheck = "Have error: " + e;
            e.printStackTrace();
        }
    }

    public String sqlInsertCheck() {
        sqlInsert();
        return sqlCheck;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getCvsSplitBy() {
        return cvsSplitBy;
    }

    public void setCvsSplitBy(String cvsSplitBy) {
        this.cvsSplitBy = cvsSplitBy;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setDataSource(SimpleDriverDataSource dataSource) {
        this.dataSource = dataSource;
    }
}

