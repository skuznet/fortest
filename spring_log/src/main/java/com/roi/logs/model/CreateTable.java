package com.roi.logs.model;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

public class CreateTable {
    private static String result;
    private JdbcTemplate jdbcTemplate;
    private SimpleDriverDataSource dataSource;

    public CreateTable(SimpleDriverDataSource dataSource, JdbcTemplate jdbcTemplate) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
    }

    public String createTable() {
        try {
            jdbcTemplate.execute("DROP TABLE IF EXISTS logs");
            jdbcTemplate.execute("create table logs(ID INT NOT NULL,"
                    + " user MEDIUMTEXT NOT NULL, url MEDIUMTEXT NOT NULL, timespent INT NOT NULL)");
            result = "Table Created.";
        } catch (Exception e) {
            result = "Error" + e;
            e.printStackTrace();
        }
        return result;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public SimpleDriverDataSource getDataSource() {
        return dataSource;
    }
}
