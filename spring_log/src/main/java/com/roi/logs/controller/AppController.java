package com.roi.logs.controller;

import com.roi.logs.model.CreateTable;
import com.roi.logs.model.StockData;
import com.roi.logs.model.TableObjectInterface;
import com.roi.logs.model.ViewDataFromTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {
    @Autowired
    StockData stockData;
    @Autowired
    CreateTable createTable;
    @Autowired
    ViewDataFromTable viewDataFromTable;
    @Autowired
    @Qualifier("logview")
    TableObjectInterface tableObjectInterface;

    @RequestMapping("/insert")
    public String index(Model model) {
        model.addAttribute("index", stockData.sqlInsertCheck());
        return "index";
    }

    @RequestMapping("/createtable")
    public String createTable(Model model) {
        model.addAttribute("createtable", createTable.createTable());
        return "createtable";
    }

    @RequestMapping("/")
    public String selectData(Model model) {
        model.addAttribute("items", viewDataFromTable.selectWholeTable(tableObjectInterface));
        return "select";
    }
}
